import { Component } from 'react'
import Phaser from 'phaser'
import { IonPhaser } from '@ion-phaser/react'
import { GameComponent } from 'phaser-react-tools'

export default class GameComponentWrapper extends Component {
    state = {
        initialize: true,
        game: {
            type: Phaser.AUTO,
            width: "100%",
            height: "100%",
            scale: {
                mode: Phaser.Scale.FIT,
                autoCenter: Phaser.Scale.CENTER_BOTH,
            },
            physics: {
                default: 'arcade',
                arcade: {
                    gravity: { y: 200 }
                }
            },
            scene: {
                init: function (this: any) {
                    this.cameras.main.setBackgroundColor('#24252A')
                },
                create: function (this: any) {
                    this.helloWorld = this.add.text(
                        this.cameras.main.centerX,
                        this.cameras.main.centerY,
                        "Hello World, prueba con un texto muchisisisimo más largo",
                        {
                            font: "40px Arial",
                            fill: "#ffffff",
                        }
                    );
                    this.helloWorld.setOrigin(0.5);
                },
                update: function (this: any) {
                    this.helloWorld.angle += 1;
                    // console.log(this.delta);
                }
            }
        }
    }

    render() {
        const { initialize, game } = this.state;
        return (
            <IonPhaser game={game} initialize={initialize} />
        );
    }
}