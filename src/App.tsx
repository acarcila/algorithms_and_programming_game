import React, { useState } from 'react';
import './App.css';
import Game from './game/GameComponent';

function App() {
  const [test, setTest] = useState(0);
  return (
    <div className="App">
      <div className="Game">
        <Game/>
      </div>
      <div className="Otro">
        Otro div
      </div>
    </div>
  );
}

export default App;
